#!/usr/bin/env python3
"""
Programma dat voor een bepaald project de planning laat zien op een bepaald punt of een bepaald gedeelte van het project
    doel: tot in detail de planning van het project duidelijk maken
    output: een website.
"""

# IMPORTS
import sys
import collections

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "0.1"
__status__ = "test"

# GLOBAL VARIABLES

# per werkpakket: de weken dat die loopt,
#                 naam van het werkpakket,
#                 het doel van het werkpakket,
#                 deelnemers van het werkpakket.
wp_dict = collections.OrderedDict(
    [("1", [["10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"],
           "management en communicatie",
           "Dit werkpakket heeft als doel het project succesvol uit te voeren binnen de gestelde termijn.<br/> "
           "Daarnaast wordt de communicatie van de (voorlopige) resultaten richting belanghebbenden vanuit dit werkpakket uitgevoerd.",
           ["Wendy van der Meulen, Wisse vogel"]]),
    ("2", [["11", "12"], "literatuuronderzoek anthocyanen hanze",
           "Het doel van dit werkpakket is het leren begrijpen van de anthocyanen die al op het lab van de opdrachtgever "
           "(kenniscentrum Biobased Economy van de Hanzehogeschool te Groningen) worden gebruikt.",
           ["Wendy van der Meulen", "Wisse Vogel"]]),
    ("3", [["13", "14", "15", "16", "17", "18"],
           "literatuuronderzoek enzymmodificatie anthocyanen",
           "In dit werkpakket wordt gekeken of anthocyanen m.b.v. enzymmodificatie kunnen worden omgezet in andere (meer bruikbare) anthocyanen.",
           ["Wendy van der Meulen", "Wisse Vogel"]]),
    ("4", [["18", "19", "20", "21"], "literatuuronderzoek toepassen enzymmodificatie in de industrie",
           "in dit werkpakket wordt gekeken of industri&euml;le processen met anthocyanen kunnen worden aangepast om te werken met enzymmodificatie om zo eventueel de kosten te drukken.",
           ["Wisse Vogel"]]),
    ("5", [["22", "23", "24", "25"], "presentatie bevindingen",
           "in dit werkpakket worden alle bevindingen besproken en duidelijk op papier/in een presentatie gezet.",
           ["Wendy van der Meulen", "Wisse Vogel"]])])
# per subdoel: werkpakket waar hij bij hoort,
#              weken dat het subdoel loopt,
#              naam van het subdoel en
#              door wie het wordt uitgevoerd.
sub_dict = {
    "1_1": ["1", ["12", "14", "16", "18", "20", "22", "24"],
            "controle proces deelnemers",
            ["Wendy van der Meulen", "Wisse Vogel"]],
    "1_2": ["1", ["13", "16", "22", "25"],
            "afhandeling verzamelde data",
            ["Wendy van der Meulen"]],
    "1_3": ["1", ["15", "16", "17", "18", "19",
                  "20", "21", "22", "23", "24", "25"],
            "ontwikkelen en bijhouden website",
            ["Wendy van der Meulen"]],
    "1_4": ["1", ["13", "17", "22", "25"],
            "communiceren resultaten",
            ["Wendy van der Meulen"]],
    "2_1": ["2", ["11", "12"],
            "metabole routes specifieke anthocyanen uitzoeken",
            ["Wendy van der Meulen"]],
    "2_2": ["2", ["11", "12"],
            "structuur uitzoeken specifieke anthocyanen",
            ["Wendy van der Meulen"]],
    "2_3": ["2", ["11", "12"],
            "Werking in de cel anthocyanen uitzoeken",
            ["Wisse Vogel"]],
    "3_1": ["3", ["13", "15"],
            "welke enzymen lijken op de enzymen gebruikt in de metabole routes wp2",
            ["Wendy van der Meulen"]],
    "3_2": ["3", ["15"],
            "wat is er in de wetenschap gedaan met enzymmodificatie m.b.t. anthocyanen",
            ["Wendy van der Meulen"]],
    "3_3": ["3", ["18"],
            "hoe gaat enzymmodificatie in zijn werk",
            ["Wisse Vogel"]],
    "4_1": ["4", ["18"],
            "industri&euml;le productie anthocyanen uitzoeken",
            ["Wisse Vogel"]],
    "4_2": ["4", ["19", "20"],
            "onderzoeken optimalisatie industri&euml;el proces anthocyanen m.b.v. enzymmodificatie",
            ["Wisse Vogel"]],
    "4_3": ["4", ["20", "21"],
            "onderzoeken hoe industrieel proces te optimaliseren m.b.v. enzymmodificatie",
            ["Wisse Vogel"]],
    "5_1": ["5", ["22"],
            "Bevindingen bespreken",
            ["Wendy van der Meulen", "Wisse Vogel"]],
    "5_2": ["5", ["22", "23"],
            "Rapport maken van bevindingen",
            ["Wendy van der Meulen", "Wisse Vogel"]],
    "5_3": ["5", ["24", "25"],
            "Eindpresentatie in elkaar zetten",
            ["Wendy van der Meulen", "Wisse Vogel"]]
}


# FUNCTIONS
def get_arguments():
    """
    doel: het ophalen en checken van de argumenten die zijn meegegeven.

    precondities: -
    postcondities: -

    input: -
    output: argument
    """
    # args = cgi.FieldStorage()
    # if args.getvalue("week"):
    #     week = args.getvalue("week")
    #     if week not in ["10", "11", "12", "13", "14",
    #                     "15", "16", "17", "18", "19",
    #                     "20", "21", "22", "23", "24", "25"]:
    #         return "invalid argument"
    #     else:
    #         return week
    # elif args.getvalue("wp"):
    #     wp = args.getvalue("wp")
    #     if wp not in ["1", "2", "3", "4", "5"]:
    #         return "invalid argument"
    #     else:
    #         return wp
    # else:
    #     return "please provide a valid argument."


def zoek_wp_bij_week(week):
    """
    doel: opzoeken welke werkpakketten er in een meegegeven week lopen.

    input: een week nummer week
    output: een lijst met werkpakketten
    """
    werkpakketten = list()

    for wp_key, wp_value in wp_dict.items():
        if week in wp_value[0]:
            werkpakketten.append(wp_key)

    return werkpakketten


def zoek_subdoelen(werkpakketten, week="NULL"):
    """
    doel: het zoeken van de subdoelen bij de gegeven werkpakketnummers en eventueel die tijd.

    input: lijst met werkpakketten wps; week="NULL" wk
    output: lijst met keys naar subdoelstellingen van sub_dict.
    """
    subs = []

    for sub_key, sub_value in sub_dict.items():
        if sub_value[0] in werkpakketten:
            if week != "NULL":
                if week in sub_value[1]:
                    subs.append(sub_key)
            else:
                subs.append(sub_key)

    return subs


def koppel_gegevens(subs, werkpakketten):
    """
    doel: de gevonden werkpakketten koppelen aan de gevonden subdoelstellingen en alle informatie die daarbij hoort.

    input: lijst met keys naar subdoelstellingen, subs; lijst met werkpakketten, werkpakketten
    output: een lijst met dictionairies waarbij werkpakketnummers de keys zijn en alle info die daarbij hoort de values.
    """
    out = []
    for wp in werkpakketten:
        temp_wp_dict = dict()
        temp_wp_dict[wp] = wp_dict[wp]

        for sub_doel in subs:
            if sub_doel[0] == wp:
                if len(temp_wp_dict[wp]) == 5:
                    temp_wp_dict[wp][4].append(sub_dict[sub_doel][1:])    # lijst met info van subdoelstellingen.
                else:
                    temp_wp_dict[wp].append([])
                    temp_wp_dict[wp][4].append(sub_dict[sub_doel][1:])

        if len(temp_wp_dict[wp]) == 4:  # geen subdoelen gevonden
            temp_wp_dict[wp].append("Geen lopende subdoelen op dit moment. Maar het hoofd werkpakket loopt nog wel.")

        out.append(temp_wp_dict)
    return out


def write_webpage(info, wp="NULL", week="NULL"):
    """
    Writes an HTML page in a certain way.
    """
    colors = {"1": ["success"],
              "2": ["secondary"],
              "3": ["warning"],
              "4": ["danger"],
              "5": ["primary"]}
    print("Content-type: text/html\n\n")
    with open("template.html") as template:
        for line in template:
            if line.strip() != "<!--inhoud-->":
                print(line)
            else:
                if wp != "NULL":
                    info_wp = list(info[0].values())[0]
                    color_wp = colors[wp][0]
                    weken_wp = info_wp[0]
                    naam_wp = info_wp[1]
                    doel_wp = info_wp[2]
                    deelnemers_wp = info_wp[3]
                    subs = info_wp[4]
                    print("<h2>Gegevens over werkpakket {}:</h2>".format(wp))
                    print('''<table class="table table-{0}" style="color: black">
                    <thead>
                      <tr>
                        <th><a href="../werkpakketten/wp{1}">Werkpakket {1}</a>: {2}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Doel van dit werkpakket: {3}</td>
                      </tr>
                        <td>Weken waarin dit werkpakket loopt: {4}</td>
                      </tr>
                      <tr>
                        <td>Deelnemers van dit werkpakket: {5}</td>
                      </tr>
                      <tr>
                        <td><h3>Subdoelstellingen:</h3></td>
                      </tr>
                    '''.format(color_wp, wp, naam_wp, doel_wp, ", ".join(weken_wp), ", ".join(deelnemers_wp)))
                    for sub_doel in subs:
                        sub_weken = sub_doel[0]
                        sub_naam = sub_doel[1]
                        sub_deelnemers = sub_doel[2]
                        print('''
                        <tr>
                          <td>
                            <table>
                              <tbody>
                                <tr>
                                  <td><h4>{0}</h4></td>
                                </tr>
                                <tr>
                                  <td>Weken dat dit subdoel loopt: {1}</td>
                                </tr>
                                <tr>
                                  <td>Door wie uitgevoerd: {2}</td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        '''.format(sub_naam, ", ".join(sub_weken), ", ".join(sub_deelnemers)))
                    print('''</tbody>
                            </table>
                            <p>Klik in het menu op planning om terug te gaan naar de planning pagina.<br/>
                              Hieronder een lijst met de data die bij de weeknummers horen:</p>
                            <ol start="10">
                              <li>5 maart t/m 11 maart</li>
                              <li>12 maart t/m 18 maart</li>
                              <li>19 maart t/m 25 maart</li>
                              <li>26 maart t/m 1 april</li>
                              <li>2 april t/m 8 april</li>
                              <li>9 april t/m 15 april</li>
                              <li>16 april t/m 22 april</li>
                              <li>23 april t/m 29 april</li>
                              <li>30 april t/m 6 mei</li>
                              <li>7 mei t/m 13 mei</li>
                              <li>14 mei t/m 20 mei</li>
                              <li>21 mei t/m 27 mei</li>
                              <li>28 mei t/m 3 juni</li>
                              <li>4 juni t/m 10 juni</li>
                              <li>11 juni t/m 17 juni</li>
                              <li>18 juni t/m 24 juni</li>
                            </ol>
                            ''')
                elif week != "NULL":
                    print("<h2>Werkpakketten in week {}:</h2>".format(week))
                    for wp in info:
                        for wp_key, wp_value in wp.items():
                            color_wp = colors[wp_key][0]
                            weken_wp = wp_value[0]
                            naam_wp = wp_value[1]
                            doel_wp = wp_value[2]
                            deelnemers_wp = wp_value[3]
                            subs = wp_value[4]

                            print('''<table class="table table-{0}" style="color: black">
                            <thead>
                              <tr>
                                <th><a href="../werkpakketten/wp{1}">Werkpakket {1}</a>: {2}</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Doel van dit werkpakket: {3}</td>
                              </tr>
                                <td>Weken waarin dit werkpakket loopt: {4}</td>
                              </tr>
                              <tr>
                                <td>Deelnemers van dit werkpakket: {5}</td>
                              </tr>
                              <tr>
                                <td><h3>Subdoelstellingen:</h3></td>
                              </tr>
                            '''.format(color_wp, wp_key, naam_wp, doel_wp, ", ".join(weken_wp), ", ".join(deelnemers_wp)))
                            if isinstance(subs, str):
                                print("<tr><td>{}</td></tr>".format(subs))
                            else:
                                for sub_doel in subs:
                                    sub_weken = sub_doel[0]
                                    sub_naam = sub_doel[1]
                                    sub_deelnemers = sub_doel[2]
                                    print('''
                                    <tr>
                                      <td>
                                        <table>
                                          <tbody>
                                            <tr>
                                              <td><h4>{0}</h4></td>
                                            </tr>
                                            <tr>
                                              <td>Weken dat dit subdoel loopt: {1}</td>
                                            </tr>
                                            <tr>
                                              <td>Door wie uitgevoerd: {2}</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                    '''.format(sub_naam, ", ".join(sub_weken), ", ".join(sub_deelnemers)))
                                print('''</tbody>
                                </table>
                                ''')
                    print('''
                    <p>Klik in het menu op planning om terug te gaan naar de planning pagina.<br/>
                      Hieronder een lijst met de data die bij de weeknummers horen:</p>
                    <ol start="10">
                      <li>5 maart t/m 11 maart</li>
                      <li>12 maart t/m 18 maart</li>
                      <li>19 maart t/m 25 maart</li>
                      <li>26 maart t/m 1 april</li>
                      <li>2 april t/m 8 april</li>
                      <li>9 april t/m 15 april</li>
                      <li>16 april t/m 22 april</li>
                      <li>23 april t/m 29 april</li>
                      <li>30 april t/m 6 mei</li>
                      <li>7 mei t/m 13 mei</li>
                      <li>14 mei t/m 20 mei</li>
                      <li>21 mei t/m 27 mei</li>
                      <li>28 mei t/m 3 juni</li>
                      <li>4 juni t/m 10 juni</li>
                      <li>11 juni t/m 17 juni</li>
                      <li>18 juni t/m 24 juni</li>
                    </ol>
                    ''')
                else:
                    print('<p>Er is iets fout gegaan.<br/>'
                          'klik <a href="../planning.html">hier</a> om terug te gaan naar de plannings pagina.</p>')



def with_week_arg(week):
    """
    tests program with a week as an argument.
    """
    werkpakketten = zoek_wp_bij_week(week)
    subs = zoek_subdoelen(werkpakketten, week)
    info = koppel_gegevens(subs, werkpakketten)
    write_webpage(info, week=week)


def with_wp_arg(wp):
    """
    tests program with a workpakket as an argument.
    """
    subs = zoek_subdoelen([wp])
    info = koppel_gegevens(subs, [wp])
    write_webpage(info, wp=wp)


# MAIN
def main():
    arg_week = "19"
    arg_wp = "4"
    # testen met een week als argument
    # with_week_arg(arg_week)

    # testen met werkpakket als argument
    with_wp_arg(arg_wp)

# RUN
if __name__ == '__main__':
    sys.exit(main())
