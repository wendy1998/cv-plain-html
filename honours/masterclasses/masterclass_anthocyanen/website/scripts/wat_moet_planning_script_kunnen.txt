- kunnen zien welke werkpakketten wanneer lopen en welke subdoelen daarvan.
- voor een datum alle werkpakketten met lopende subdoelen ophalen en van die subdoelen informatie geven.
- voor een bepaald werkpakket alle subdoelen laten zien en wanneer die lopen.
- een overzichtelijke HTML pagina genereren

benodigdheden daarvoor:
- dictionairy met wanneer werkpaketten lopen in weken
- dictionairy per werkpakket wanneer subdoelen lopen

- functie die de argumenten checkt/ophaalt.
naam: get_arguments
doel: het ophalen en checken van de argumenten die zijn meegegeven.

precondities: -
postcondities: -

input: -
output: argument

algoritme:
args = fieldinput
als er een week in args zit:
    als week < 10 of week > 25:
        return "not a valid week"
    return week
als er een werkpakket in args zit:
    als werkpakket > 5:
        return "not a valid workpakket."
    return werkpakket

- functie die de juiste werkpakketten bij de juiste week zoekt.
naam: zoek_werkpakket_bij_week
doel: opzoeken welke werkpakketten er in een meegegeven week lopen.

precondities: dict met wanneer werkpakketten lopen en de informatie m.b.t. die werkpakketten is algemeen beschikbaar (wp_dict),
in wp_dict zit voor elk werkpakket: een lijst met weeknummers wanneer hij loopt wk,
                                    het doel van het werkpakket dl,
                                    een lijst met deelnemers aan dat werkpakket dn.
postcondities: -

input: een week nummer n
output: een lijst met werkpakketten

algoritme:
werkpakketten = list()

voor elke value in wp_dict:
    als n in wk:
        voeg werkpakketnummer toe aan werkpakketten

return werkpakketten

- functie die de juiste subwerkpakketten bij die/dat werkpakket(en) (en evt. die week) zoekt.
naam: zoek_subdoelen
doel: het zoeken van de subdoelen bij de gegeven werkpakketnummers en eventueel die tijd.
precondities:
dict met wanneer subdoelstellingen lopen is algemeen beschikbaar (sub_dict)
per subdoelstelling in sub_dict is opgeslagen: werkpakket waar hij bij hoort wp,
                                               lijst met weken dat ie loopt wk_sub,
                                               naam nm,
                                               door wie hij uitgevoerd wordt dn_sub.

postcondities: -

input: lijst met werkpakketten wps; week="NULL" wk
output: lijst met keys naar subdoelstellingen van sub_dict.

algoritme:
subs = list()

voor subdoelstelling in sub_dict:
    als wp in wps zit:
        als wk niet gelijk is aan "NULL":
            als wk in subdoelstelling[wk_sub]:
                voeg key van subdoelstelling aan subs toe.
        anders:
            voeg key van subdoelstelling aan subs toe.

return subs

- functie die alle informatie over een werkpakket en subdoelstelling ophaalt en die overzichtelijk aan elkaar koppelt.
naam: koppel_gevonden_gegevens
doel: de gevonden werkpakketten koppelen aan de gevonden subdoelstellingen en alle informatie die daarbij hoort.

precondities:
dict met wanneer subdoelstellingen lopen is algemeen beschikbaar (sub_dict)
per subdoelstelling in sub_dict is opgeslagen: werkpakket waar hij bij hoort wp,
                                               lijst met weken dat ie loopt wk_sub,
                                               naam nm,
                                               door wie hij uitgevoerd wordt dn_sub.

dict met wanneer werkpakketten lopen en de informatie m.b.t. die werkpakketten is algemeen beschikbaar (wp_dict),
in wp_dict zit voor elk werkpakket: een lijst met weeknummers wanneer hij loopt wk,
                                    het doel van het werkpakket dl,
                                    een lijst met deelnemers aan dat werkpakket dn.

postcondities: -

input: lijst met keys naar subdoelstellingen, subs; lijst met werkpakketten, werkpakketten
output: een lijst met dictionairies waarbij werkpakketnummers de keys zijn en alle info die daarbij hoort de values.

algoritme:
out = []

voor workpakket in werkpakketten:
    temp_wp_dict = {}
    temp_wp_dict[workpakket] = wp_dict[workpakket]
    voor sub_doel in subs:
        als werkpakketnummer van sub_doel gelijk is aan workpakket:
            als lijst met subdoel_info al bestaat in temp_wp_dict:
                voeg de naam van die subdoelstelling en lijst met weken dat dat subdoel loopt toe aan lijst met subdoel_info in temp_wp_dict.
            anders:
                maak lijst met subdoel_info in temp_wp_dict
                voeg de naam van die subdoelstelling,
                lijst met weken dat dat subdoel loopt
                en wie het uitvoert toe aan lijst met subdoel_info in temp_wp_dict.
    voeg temp_wp_dict toe aan out.

return out
- functie die een HTML pagina genereerd.
