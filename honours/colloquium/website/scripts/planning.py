#!/usr/bin/env python3
"""
Programma dat voor een bepaald project de planning laat zien op een bepaald punt of een bepaald gedeelte van het project
    doel: tot in detail de planning van het project duidelijk maken
    output: een website.
"""

# IMPORTS
import sys
import collections

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "0.1"
__status__ = "test"

# GLOBAL VARIABLES

# per werkpakket: de weken dat die loopt,
#                 naam van het werkpakket,
#                 het doel van het werkpakket
wp_dict = collections.OrderedDict(
    [("1", [["7", "8", "9", "10", "11", ">"],
            "Management",
            "This workingpacket has the purpose to make sure that the project comes to an end "
            "in the right timeframe and with desirable results."]),
     ("2", [["7", "8"], "Modern Pharaohs",
            "This workingpacket has the purpose to find out how pharaohs lived and "
            "what of that can still be seen in the human genome."]),
     ("3", [["7", "8", "9"],
            "Mixing of human species",
            "This workingpacket has the purpose to find out "
            "how human species mixed back when there were plenty. "]),
     ("4", [["9", "10", "11"], "Tracing the origin of modern humans",
            "This workingpacket has the purpose to trace the modern "
            "day humans back to their ancestors as far as possible. "]),
     ("5", [["7", "8", "9", "10", "11", ">"], "Presentating and logging",
            "This workingpacket has the purpose to log everything "
            "in a report and a time logbook and to create a presentation."])])

# per subdoel: werkpakket waar hij bij hoort,
#              weken dat het subdoel loopt,
#              naam van het subdoel.
sub_dict = {
    "1_1": ["1", ["7"],
            "Making plan of action"],
    "1_2": ["1", ["7", "9", "11", ">"],
            "Communication: creating/updating website on the project"],
    "2_1": ["2", ["7", "8"],
            "Looking up scientific articles having to do with phylogenetics and the pharaohs"],
    "2_2": ["2", ["8"],
            "Finding traces of their lives in our genomes"],
    "3_1": ["3", ["7", "8", "9"],
            "Mixings with Neanderthals"],
    "3_2": ["3", ["7", "8", "9"],
            "Mixings with other species."],
    "4_1": ["4", ["9"],
            "Finding the most recent common ancestor of modern humans and Neanderthals"],
    "4_2": ["4", ["9"],
            "The evolution of human-specific traits"],
    "4_3": ["4", ["10"],
            "Human population variation in immune responses"],
    "4_4": ["4", ["10", "11"],
            "Evolutionary history around the world"],
    "5_1": ["5", ["7", "8", "9", "10", "11", ">"],
            "Keep time logbook"],
    "5_2": ["5", [">"],
            "Create scientific report with findings"],
    "5_3": ["5", [">"],
            "Create final presentation"]
}


# FUNCTIONS
def get_arguments():
    """
    doel: het ophalen en checken van de argumenten die zijn meegegeven.

    precondities: -
    postcondities: -

    input: -
    output: argument
    """
    # args = cgi.FieldStorage()
    # if args.getvalue("week"):
    #     week = args.getvalue("week")
    #     if week not in ["7", "8", "9", "10", "11", ">"]:
    #         return "invalid argument"
    #     else:
    #         return week
    # elif args.getvalue("wp"):
    #     wp = args.getvalue("wp")
    #     if wp not in ["1", "2", "3", "4", "5"]:
    #         return "invalid argument"
    #     else:
    #         return wp
    # else:
    #     return "please provide a valid argument."


def zoek_wp_bij_week(week):
    """
    doel: opzoeken welke werkpakketten er in een meegegeven week lopen.

    input: een week nummer week
    output: een lijst met werkpakketten
    """
    werkpakketten = list()

    for wp_key, wp_value in wp_dict.items():
        if week in wp_value[0]:
            werkpakketten.append(wp_key)

    return werkpakketten


def zoek_subdoelen(werkpakketten, week="NULL"):
    """
    doel: het zoeken van de subdoelen bij de gegeven werkpakketnummers en eventueel die tijd.

    input: lijst met werkpakketten wps; week="NULL" wk
    output: lijst met keys naar subdoelstellingen van sub_dict.
    """
    subs = []

    for sub_key, sub_value in sub_dict.items():
        if sub_value[0] in werkpakketten:
            if week != "NULL":
                if week in sub_value[1]:
                    subs.append(sub_key)
            else:
                subs.append(sub_key)

    return subs


def koppel_gegevens(subs, werkpakketten):
    """
    doel: de gevonden werkpakketten koppelen aan de gevonden subdoelstellingen en alle informatie die daarbij hoort.

    input: lijst met keys naar subdoelstellingen, subs; lijst met werkpakketten, werkpakketten
    output: een lijst met dictionairies waarbij werkpakketnummers de keys zijn en alle info die daarbij hoort de values.
    """
    out = []
    for wp in werkpakketten:
        temp_wp_dict = dict()
        temp_wp_dict[wp] = wp_dict[wp]

        for sub_doel in subs:
            if sub_doel[0] == wp:
                if len(temp_wp_dict[wp]) == 4:
                    temp_wp_dict[wp][3].append(sub_dict[sub_doel][1:])    # lijst met info van subdoelstellingen.
                else:
                    temp_wp_dict[wp].append([])
                    temp_wp_dict[wp][3].append(sub_dict[sub_doel][1:])

        if len(temp_wp_dict[wp]) == 3:  # geen subdoelen gevonden
            temp_wp_dict[wp].append("No subobjectives run at the moment, but the main objective still runs.")

        out.append(temp_wp_dict)
    return out


def write_webpage(info, wp=None, week=None):
    """
    Writes an HTML page in a certain way.
    """
    colors = {"1": ["success"],
              "2": ["secondary"],
              "3": ["warning"],
              "4": ["danger"],
              "5": ["primary"]}
    print("Content-type: text/html\n\n")
    with open("template.html") as template:
        for line in template:
            if line.strip() != "<!--inhoud-->":
                print(line)
            else:
                if wp:
                    info_wp = list(info[0].values())[0]
                    color_wp = colors[wp][0]
                    weken_wp = info_wp[0]
                    naam_wp = info_wp[1]
                    doel_wp = info_wp[2]
                    subs = info_wp[3]
                    print("<h2>Workingpacket {}:</h2>".format(wp))
                    print('''<table class="table table-{0} text-black">
                                <thead>
                                  <tr>
                                    <th><a href="../werkpakketten/wp{1}">Workingpacket {1}</a>: {2}</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Purpose of this workingpacket: {3}</td>
                                  </tr>
                                    <td>Weeks that this workingpacket runs in: {4}</td>
                                  </tr>
                                  <tr>
                                    <td><h3>subobjectives:</h3></td>
                                  </tr>
                                '''.format(color_wp, wp, naam_wp, doel_wp, ", ".join(weken_wp)))
                    for sub_doel in subs:
                        sub_weken = sub_doel[0]
                        sub_naam = sub_doel[1]
                        print('''
                                <tr>
                                  <td>
                                    <table>
                                      <tbody>
                                        <tr>
                                          <td><h4>{0}</h4></td>
                                        </tr>
                                        <tr>
                                          <td>Weeks that this subobjective runs in: {1}</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                                '''.format(sub_naam, ", ".join(sub_weken)))
                    print('''</tbody>
                            </table>
                            <p>Click in the menu on planning to return to the planning page.<br/>
                              A list with the dates that correspond to the week numbers:</p>
                            <ol start="7">
                              <li>11-02-2019 to 17-02-2019</li>
                              <li>18-02-2019 to 24-02-2019</li>
                              <li>25-02-2019 to 03-03-2019</li>
                              <li>04-03-2019 to 11-03-2019</li>
                              <li>12-03-2019 to 15-03-2019</li>
                            </ol>
                            <p>Whenever there is a > in one of the listed weeks, 
                            that means just before the presentation, 
                            which takes place 3 months after the end of the primary research.</p>
                            ''')
                elif week:
                    print("<h2>Working packets in week {}:</h2>".format(week))
                    for wp in info:
                        for wp_key, wp_value in wp.items():
                            color_wp = colors[wp_key][0]
                            weken_wp = wp_value[0]
                            naam_wp = wp_value[1]
                            doel_wp = wp_value[2]
                            subs = wp_value[3]
                            print('''<table class="table table-{0} text-black">
                                        <thead>
                                          <tr>
                                            <th><a href="../werkpakketten/wp{1}">Workingpacket {1}</a>: {2}</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td>Purpose of this workingpacket: {3}</td>
                                          </tr>
                                            <td>Weeks that this workingpacket runs in: {4}</td>
                                          </tr>
                                          <tr>
                                            <td><h3>subobjectives:</h3></td>
                                          </tr>
                                        '''.format(color_wp, wp_key, naam_wp, doel_wp, ", ".join(weken_wp)))
                            if isinstance(subs, str):
                                print("<tr><td>{}</td></tr>".format(subs))
                            else:
                                for sub_doel in subs:
                                    sub_weken = sub_doel[0]
                                    sub_naam = sub_doel[1]
                                    print('''
                                        <tr>
                                          <td>
                                            <table>
                                              <tbody>
                                                <tr>
                                                  <td><h4>{0}</h4></td>
                                                </tr>
                                                <tr>
                                                  <td>Weeks that this subobjective runs in: {1}</td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                        '''.format(sub_naam, ", ".join(sub_weken)))
                                print('''</tbody>
                                </table>
                                ''')
                    print('''</tbody>
                        </table>
                        <p>Click in the menu on planning to return to the planning page.<br/>
                          A list with the dates that correspond to the week numbers:</p>
                        <ol start="7">
                          <li>11-02-2019 to 17-02-2019</li>
                          <li>18-02-2019 to 24-02-2019</li>
                          <li>25-02-2019 to 03-03-2019</li>
                          <li>04-03-2019 to 11-03-2019</li>
                          <li>12-03-2019 to 15-03-2019</li>
                        </ol>
                        <p>Whenever there is a > in one of the listed weeks, that means just before the presentation, 
                        which takes place 3 months after the end of the primary research.</p>
                        ''')
                else:
                    print('<p>Something went wrong.<br/>'
                          'click <a href="../planning.html">here</a> to go back to the planning page.</p>')


def with_week_arg(week):
    """
    tests program with a week as an argument.
    """
    werkpakketten = zoek_wp_bij_week(week)
    subs = zoek_subdoelen(werkpakketten, week)
    info = koppel_gegevens(subs, werkpakketten)
    write_webpage(info, week=week)


def with_wp_arg(wp):
    """
    tests program with a workpakket as an argument.
    """
    subs = zoek_subdoelen([wp])
    info = koppel_gegevens(subs, [wp])
    write_webpage(info, wp=wp)


# MAIN
def main():
    arg_week = ">"
    arg_wp = "4"
    # testen met een week als argument
    with_week_arg(arg_week)

    # testen met werkpakket als argument
    # with_wp_arg(arg_wp)


# RUN
if __name__ == '__main__':
    sys.exit(main())
