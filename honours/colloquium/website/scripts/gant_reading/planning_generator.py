#!/usr/bin/env python3
"""
    A module that can read an xlsx file with a gant chart in it and turn it into a planning.
        Usage: Either run it from the commandline with an
               xlsx sheet or run the program that creates a planning and call it from there.
"""

# IMPORTS
import openpyxl as xl
import sys
import argparse
import collections
import string

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = 0.1
__status__ = "incomplete"

# GLOBAL VARIABLES
alphabet = string.ascii_uppercase


# CLASSES
class PlanningGenerator:
    def __init__(self, file, start_pos_objectives, start_pos_wp_names, column_week_start, column_week_end, sheet):
        self.workbook = xl.load_workbook(file)
        self.sheet = self.workbook[sheet]
        self.working_packets = []
        self.week_idxs = self.index_weeks(column_week_start, column_week_end)
        self.cols_weeks = list(self.week_idxs.keys())
        self.kopple_info(start_pos_objectives, start_pos_wp_names)

    def index_weeks(self, start_col, end_col):
        idxs = collections.OrderedDict()
        row = str(self.sheet[start_col].row)

        for character in list(map(chr, range(ord(start_col[0]), ord(end_col[0])+1))):
            idxs[str(character)] = str(self.sheet["".join([character, row])].value)

        return idxs

    @staticmethod
    def create_subobjective(working_packet, name, weeks):
        working_packet.add_objective(name, weeks)

    def find_filled_weeks(self, row):
        filled = []
        positions = ["".join([col, str(row)]) for col in self.cols_weeks]
        for position in positions:
            cell = self.sheet[position]
            if cell.fill.start_color.index != "00000000":
                filled.append(self.week_idxs[position[0]])

        return filled

    def kopple_info(self, pos_objct, pos_wp_names):
        col_objcts = alphabet[self.sheet[pos_objct].column - 1]
        col_names = alphabet[self.sheet[pos_wp_names].column - 1]
        start_row = self.sheet[pos_objct].row

        for i in range(start_row, 10000):
            objctv = self.sheet["".join([col_objcts, str(i)])]

            if not objctv.value:
                break

            wp_name = self.sheet["".join([col_names, str(i)])]
            filled_weeks = self.find_filled_weeks(i)

            if wp_name.value:
                wp_purpose = self.extract_purpose(objctv.value)
                wp_num = self.extract_wp_num(objctv.value)
                wp = WorkingPacket(wp_num, wp_name.value, wp_purpose, filled_weeks)
                self.working_packets.append(wp)
            else:
                sub_objctv = Objective(objctv.value, filled_weeks)
                self.working_packets[-1].add_objective(sub_objctv)

    @staticmethod
    def extract_purpose(cell_value):
        purpose = cell_value.split(": ")[1]

        return purpose

    @staticmethod
    def extract_wp_num(cell_value):
        num = []
        search = cell_value.split(":")[0]

        for character in search:
            if character in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
                num.append(character)

        return "".join(num)

    def get_working_packets(self):
        return self.working_packets


class WorkingPacket:
    def __init__(self, num, name, purpose, weeks):
        self.num = num
        self.name = name
        self.weeks = weeks
        self.purpose = purpose
        self.objectives = []

    def __str__(self):
        out = ["number: {4}{1}name: {0}{1} "
               "weeks it runs in: {2}{1}"
               "purpose: {3}{1}sub objectives: {1}".format(self.name, "\n", ", ".join(self.weeks),
                                                           self.purpose, self.num)]
        for count, objective in enumerate(self.objectives):
            out.append("{}. {}\n\tweeks it runs in: {}\n".format(count+1, objective.get_name(),
                                                                 ", ".join(objective.get_weeks())))

        return "".join(out)

    def add_objective(self, objective):
        self.objectives.append(objective)

    # setters
    def set_objectives(self, objectives):
        self.objectives = objectives

    # getters
    def get_name(self):
        return self.name

    def get_weeks(self):
        return self.weeks

    def get_purpose(self):
        return self.purpose

    def get_objectives(self, week=None):
        if not week:
            return self.objectives
        else:
            out = []
            for objective in self.objectives:
                if week in objective.get_weeks():
                    out.append(objective)

            if out:
                return out
            else:
                return "No sub objectives are running right now, but the head objective still runs."

    def get_num(self):
        return self.num


class Objective:
    def __init__(self, name, weeks):
        self.name = name
        self.weeks = weeks

    # getters
    def get_name(self):
        return self.name

    def get_weeks(self):
        return self.weeks


# DEFINITIONS
def parse_args():
    """Parses arguments from the commandline"""
    parser = argparse.ArgumentParser()

    # adding possible arguments to the parser
    parser.add_argument("-wb", "--workbook", help="Place the xlsx file with the gant chart in here.", required=True)
    parser.add_argument("-cwp", "--workingpackets", help="Place the index of the column and row "
                                                         "where the workingpackets and objectives start in here "
                                                         "(for example B3)", required=True)
    parser.add_argument("-cn", "--names",
                        help="Place the index of the column and row where the names of the workingpackets "
                             "start in here (for example C3)", required=True)
    parser.add_argument("-wks", "--wkstart", help="Place the index of the column and row where the first week number "
                                                  "is located in here", required=True)
    parser.add_argument("-wke", "--wkend", help="Place the index of the column and row where the last week number "
                                                "is located in here (must still fall in the single letter range, "
                                                "otherwise this will not work.", required=True)
    parser.add_argument("-stn", "--sheet", help="Place the name of the sheet with the chart in here. "
                                                "(optional, default = 'Blad1')", default="Blad1")

    # saving all the variables
    args = parser.parse_args()

    return args


def extract_row(pos):
    row = []
    for character in pos:
        if character in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
            row.append(character)

    return "".join(row)


def check_args(args):
    row_names = extract_row(args.names)
    row_objctv = extract_row(args.workingpackets)
    row_weeks = str(int(extract_row(args.wkstart))+1)

    if row_names != row_objctv or row_names != row_weeks:
        print("Please make sure that the names of the workingpackets "
              "start in the same row as the objectives and the weeks. Exiting now...")
        sys.exit(1)

    return 0


def main():
    args = parse_args()
    check_args(args)

    pg = PlanningGenerator(args.workbook, args.workingpackets, args.names, args.wkstart, args.wkend, args.sheet)
    workingpackets = pg.get_working_packets()

    for workingpacket in workingpackets:
        print(workingpacket)

    return 0


if __name__ == '__main__':
    sys.exit(main())
