#!/usr/bin/env python3
"""
Programma dat voor een bepaald project de planning laat zien op een bepaald punt of een bepaald gedeelte van het project
    doel: tot in detail de planning van het project duidelijk maken
    output: een website.
"""

# IMPORTS
import sys
import cgi
from planning_generator import PlanningGenerator

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "0.1"
__status__ = "test"

# GLOBAL VARIABLES

pg = PlanningGenerator("gantt_chart.xlsx", "B3", "C3", "D2", "I2", "Blad1")


# FUNCTIONS
def get_arguments():
    """
    doel: het ophalen en checken van de argumenten die zijn meegegeven.

    precondities: -
    postcondities: -

    input: -
    output: argument
    """
    args = cgi.FieldStorage()
    if args.getvalue("week"):
        week = args.getvalue("week")
        if week not in ["7", "8", "9", "10", "11", ">"]:
            return "invalid argument"
        else:
            return week
    elif args.getvalue("wp"):
        wp = args.getvalue("wp")
        if wp not in ["1", "2", "3", "4", "5"]:
            return "invalid argument"
        else:
            return wp
    else:
        return "please provide a valid argument."


def zoek_wp_bij_week(week):
    """
    doel: opzoeken welke werkpakketten er in een meegegeven week lopen.

    input: een week nummer week
    output: een lijst met werkpakketten
    """
    werkpakketten = list()

    for workingpacket in pg.get_working_packets():
        if week in workingpacket.get_weeks():
            werkpakketten.append(workingpacket)

    return werkpakketten


def write_webpage(wp=None, week=None):
    """
    Writes an HTML page in a certain way.
    """
    colors = {"1": ["success"],
              "2": ["secondary"],
              "3": ["warning"],
              "4": ["danger"],
              "5": ["primary"]}
    print("Content-type: text/html\n\n")
    with open("template.html") as template:
        for line in template:
            if line.strip() != "<!--inhoud-->":
                print(line)
            else:
                if week:
                    print("<h2>Working packets in week {}:</h2>".format(week))
                    for workingpacket in wp:
                        color_wp = colors[workingpacket.get_num()][0]
                        weken_wp = workingpacket.get_weeks()
                        naam_wp = workingpacket.get_name()
                        doel_wp = workingpacket.get_purpose()
                        subs = workingpacket.get_objectives(week)
                        print('''<table class="table table-{0} text-black">
                                    <thead>
                                      <tr>
                                        <th><a href="../werkpakketten/wp{1}">Workingpacket {1}</a>: {2}</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>Purpose of this workingpacket: {3}</td>
                                      </tr>
                                        <td>Weeks that this workingpacket runs in: {4}</td>
                                      </tr>
                                      <tr>
                                        <td><h3>subobjectives:</h3></td>
                                      </tr>
                                    '''.format(color_wp, workingpacket.get_num(), naam_wp,
                                               doel_wp, ", ".join(weken_wp)))
                        if isinstance(subs, str):
                            print("<tr><td>{}</td></tr>".format(subs))
                        else:
                            for sub_doel in subs:
                                sub_weken = sub_doel.get_weeks()
                                sub_naam = sub_doel.get_name()
                                print('''
                                    <tr>
                                      <td>
                                        <table>
                                          <tbody>
                                            <tr>
                                              <td><h4>{0}</h4></td>
                                            </tr>
                                            <tr>
                                              <td>Weeks that this subobjective runs in: {1}</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                    '''.format(sub_naam, ", ".join(sub_weken)))
                            print('''</tbody>
                            </table>
                            ''')
                    print('''</tbody>
                                </table>
                                <p>Click in the menu on planning to return to the planning page.<br/>
                                  A list with the dates that correspond to the week numbers:</p>
                                <ol start="7">
                                  <li>11-02-2019 to 17-02-2019</li>
                                  <li>18-02-2019 to 24-02-2019</li>
                                  <li>25-02-2019 to 03-03-2019</li>
                                  <li>04-03-2019 to 11-03-2019</li>
                                  <li>12-03-2019 to 15-03-2019</li>
                                </ol>
                                <p>Whenever there is a > in one of the listed weeks, that means just before the presentation,
                                which takes place 3 months after the end of the primary research.</p>
                                ''')
                elif wp:
                    color_wp = colors[wp.get_num()][0]
                    weken_wp = wp.get_weeks()
                    naam_wp = wp.get_name()
                    doel_wp = wp.get_purpose()
                    subs = wp.get_objectives()
                    print("<h2>Workingpacket {}:</h2>".format(wp))
                    print('''<table class="table table-{0} text-black">
                    <thead>
                      <tr>
                        <th><a href="../werkpakketten/wp{1}">Workingpacket {1}</a>: {2}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Purpose of this workingpacket: {3}</td>
                      </tr>
                        <td>Weeks that this workingpacket runs in: {4}</td>
                      </tr>
                      <tr>
                        <td><h3>subobjectives:</h3></td>
                      </tr>
                    '''.format(color_wp, wp.get_num(), naam_wp, doel_wp, ", ".join(weken_wp)))
                    for sub_doel in subs:
                        sub_weken = sub_doel.get_weeks()
                        sub_naam = sub_doel.get_name()
                        print('''
                        <tr>
                          <td>
                            <table>
                              <tbody>
                                <tr>
                                  <td><h4>{0}</h4></td>
                                </tr>
                                <tr>
                                  <td>Weeks that this subobjective runs in: {1}</td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        '''.format(sub_naam, ", ".join(sub_weken)))
                    print('''</tbody>
                            </table>
                            <p>Click in the menu on planning to return to the planning page.<br/>
                              A list with the dates that correspond to the week numbers:</p>
                            <ol start="7">
                              <li>11-02-2019 to 17-02-2019</li>
                              <li>18-02-2019 to 24-02-2019</li>
                              <li>25-02-2019 to 03-03-2019</li>
                              <li>04-03-2019 to 11-03-2019</li>
                              <li>12-03-2019 to 15-03-2019</li>
                            </ol>
                            <p>Whenever there is a > in one of the listed weeks, that means just before the presentation,
                            which takes place 3 months after the end of the primary research.</p>
                            ''')
                else:
                    print('<p>Something went wrong.<br/>'
                          'click <a href="../planning.html">here</a> to go back to the planning page.</p>')


def with_week_arg(week):
    """
    tests program with a week as an argument.
    """
    werkpakketten = zoek_wp_bij_week(week)
    write_webpage(werkpakketten, week)


def find_wp_object(wp):
    for workingpacket in pg.get_working_packets():
        if wp == workingpacket.get_num():
            return workingpacket


def with_wp_arg(wp):
    """
    tests program with a workingpacket as an argument.
    """
    workingpacket = find_wp_object(wp)
    write_webpage(wp=workingpacket)


# MAIN
def main():
    arg = get_arguments()
    if "argument" in arg:
        write_webpage(1)
    elif arg == ">" or int(arg) > 5:
        with_week_arg(arg)
    else:
        with_wp_arg(arg)


# RUN
if __name__ == '__main__':
    sys.exit(main())
