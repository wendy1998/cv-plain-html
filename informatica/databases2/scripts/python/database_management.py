#!/usr/bin/env python3
"""
Module that talks to a relational database through mysql connector.
    Usage: import database_management
"""

# IMPORTS
import sys
import mysql.connector
import tentamen

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "0.1"
__status__ = "Module"


# CLASSES
class TalkToDatabase:
    """Class that uses mySQL connect
    to talk to a fully filled database"""
    def __init__(self, username, db_name, password, host, student="optional", course="optional"):
        self.username = self.set_username(username)
        self.db_name = self.set_database(db_name)
        self.password = self.set_password(password)
        self.host = self.set_host(host)
        self.student = student
        self.course = course
        if self.check_database():  # it's possible to make a connection
            self.cnx = mysql.connector.connect(user=self.username,
                                               database=self.db_name,
                                               password=self.password,
                                               host=self.host)
            self.cursor = self.cnx.cursor()

            # methods
            self.students = self.ask_for_students()
            self.results = self.ask_for_exam_results()
            self.cnx.close()
            self.exam_objects = self.make_exam_objects()
            self.exam_dict = self.make_exam_dict()
        else:
            print("Connection to database couldn't be made, no results fetched.")
            self.students = []
            self.exam_objects = []
            self.exam_dict = {}
    
    # setters
    def set_username(self, username):
        """Set's the username value"""
        return username

    def set_database(self, database):
        """Set's the database value"""
        return database

    def set_password(self, password):
        """Set's the password value"""
        return password

    def set_host(self, host):
        """Set's the host value"""
        return host

    def check_database(self):
        """Checks if the database can be accessed by the given credentials."""
        try:
            mysql.connector.connect(user=self.username,
                                    database=self.db_name,
                                    password=self.password,
                                    host=self.host)
        except mysql.connector.Error:
            return False

        return True

    def ask_for_students(self):
        """Get's the names of the students in the database"""
        students = []
        self.cursor.execute("select voorletters, naam from studenten order by naam asc;")
        out = self.cursor.fetchall()
        for student in out:  # student (voorletters, achternaam)
            students.append(". ".join(student))

        return students

    def ask_for_exam_results(self):
        """Gets the exam results for every student or a specific student in the database."""
        if self.student != "optional":
            voorletters = self.student.split(". ")[0]
            achternaam = self.student.split(". ")[1]
            self.cursor.execute("select voorletters, s.naam, c.naam, "
                                "ex_datum, cijfer "
                                "from studenten s join examens e join cursussen c "
                                "on s.stud_id = e.stud_id and e.cur_id = c.cur_id "
                                "where voorletters = '{}' and s.naam = '{}';".format(voorletters, achternaam))
        else:
            self.cursor.execute("select voorletters, s.naam, c.naam, "
                                "ex_datum, cijfer "
                                "from studenten s join examens e join cursussen c "
                                "on s.stud_id = e.stud_id and e.cur_id = c.cur_id;")
        out = self.cursor.fetchall()
        return out

    def make_exam_objects(self):
        """Makes objects of the results per student and puts them in a list."""
        tentamens = []
        if not self.results:
            tentamens.append(tentamen.Tentamen("NULL", "NULL", "NULL"))

        for results in self.results:  # results = (voorletters, achternaam, course, date, mark)
            if not results[4]:  # no mark
                if not results[3]:  # no date
                    tentamens.append(tentamen.Tentamen(". ".join(results[:2]), results[2], "NULL"))
                else:
                    tentamens.append(tentamen.Tentamen(". ".join(results[:2]), results[2], str(results[3])))
            else:
                if not results[3]:  # no date
                    tentamens.append(tentamen.Tentamen(". ".join(results[:2]), results[2], "NULL", results[4]))
                else:
                    tentamens.append(tentamen.Tentamen(". ".join(results[:2]), results[2], str(results[3]), results[4]))
        return tentamens

    def make_exam_dict(self):
        """Get's the exams per student and puts them in a dictionary"""
        student_results = {}
        if not self.results:
            student_results["NULL"] = "This student hasn't made any tests."

        for exam in self.results:  # exam = (voorletters, student, course, date, mark)
            student = ". ".join(exam[:2])
            if student in student_results.keys():
                student_results[student].append((exam[2], str(exam[3]), exam[4]))
            else:
                student_results[student] = []
                student_results[student].append((exam[2], str(exam[3]), exam[4]))
        return student_results

    # getters
    def get_username(self):
        """Returns the username given for the database"""
        return self.username

    def get_database(self):
        """Returns the databse name given for the database connection"""
        return self.db_name

    def get_password(self):
        """Says that you're not allowed to see the password"""
        print("You aren't allowed to see the password!")
        return 0

    def get_host(self):
        """Returns the host given for the database connection"""
        return self.host

    def get_students(self):
        """returns the names of the students in the database."""
        return self.students

    def get_exams(self):
        """returns the exam results of the students in the database in object form"""
        return self.exam_objects

    def get_results_dict(self):
        """returns the exam results of the students in the database in dictionary form"""
        return self.exam_dict


# DEFINITIONS
def check_students():
    """Checks if the fetching of the students from the database goes right."""
    if connection.get_students() != ['k. baan', 'ja. bakker', 'g. bikkel', 'cm. cals',
                                     'pk. hofman', 'r. jansen', 'w. jansen', 'e. molenbeek',
                                     'n. myra', 'a. nieuwkerk', 'am. poortinga', 'r. spaans',
                                     'd. temming', 'e. worms']:
        print("Student fetching goes wrong!")
    else:
        print("Student fetching goes right!")

    # the query I used for this was: select naam from studenten order by 1 asc;
    return 0


def check_exams():
    """Checks if exam fetching goes right"""
    for result in connection.get_exams():  # all Tentamen objects
        print(result)

    # the query that I used to fetch this gives (almost) the same result.
    # The mark that's different, I did deliberately because the date is in the future.
    # Dates that are unknown are converted to 1111-11-11.
    # query: select s.naam, c.naam, ex_datum, cijfer from studenten s join examens e join cursussen c
        # on s.stud_id = e.stud_id and e.cur_id = c.cur_id;"

    for result in connection1.get_exams():
        if [result.get_student(), result.get_vak(), result.get_datum(), result.get_cijfer()] \
                != ["NULL", "NULL", "Date unknown", "NULL"]:
            print("Exam results nonexistent student goes wrong!")
        else:
            print("Exam results nonexistent student goes right!")

    for result in connection2.get_exams():
        if result.get_student() != "e. molenbeek" or result.get_vak() != "progr. in Java" \
                or result.get_datum() != "Date unknown" or result.get_cijfer() != "NULL":
            print("Exam results existing student goes wrong!")
        else:
            print("Exam results existing student goes right!")
    return 0


# MAIN
def main():
    """Tests the class"""
    global connection, connection1, connection2
    # PREPARATIONS
    # WORK
    connection = TalkToDatabase("wbvandermeulen", "Wbvandermeulen", "tRn51Ha4", host="mysql.bin")
    connection1 = TalkToDatabase("wbvandermeulen", "Wbvandermeulen", "tRn51Ha4", host="mysql.bin", student="wb. vandermeulen")
    connection2 = TalkToDatabase("wbvandermeulen", "Wbvandermeulen", "tRn51Ha4", host="mysql.bin", student="e. molenbeek")

    # if connection couldn't be established,
    # students will be an empty list and will be converted to False in the if statement.
    # check_students and check_exams will not function properly if students is empty.
    if connection.get_students():
        check_students()
        check_exams()

    # FINISH
    return 0

# RUN
if __name__ == '__main__':
    sys.exit(main())
