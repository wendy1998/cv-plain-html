#!/usr/bin/env python3
"""
Program with a class that talks to a database through stored procedures.
Usage: ./Wendy_Meulen_deelopdracht4.py
"""

# IMPORTS
import sys
import mysql.connector

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "0.1"
__status__ = "Module"


# CLASSES
class TalkToDatabase:
    """Class that uses mySQL connect
    to talk to a fully filled database"""
    def __init__(self):
        if self.check_database():  # it's possible to make a connection
            self.cnx = mysql.connector.connect(option_files=".my.cnf")
            self.cursor = self.cnx.cursor()

            # methods
            self.gene_externals = self.ask_for_genes()
            self.meltprobes = self.ask_for_melt_probes_ratio()
            self.marked = self.mark_duplicates()
            self.cnx.close()
        else:
            print("Connection to database couldn't be made, no results fetched.")
            self.gene_externals = []
            self.meltprobes = 0
            self.marked = False

    def check_database(self):
        """Checks if the database can be accessed by the given credentials."""
        try:
            mysql.connector.connect(option_files=".my.cnf")
        except mysql.connector.Error:
            return False

        return True

    def ask_for_genes(self):
        """Asks for the external ids of the genes in the database"""
        genes = []
        self.cursor.callproc("sp_get_genes")
        out = self.cursor.fetchall()
        for gene in out:  # gene (sequentie, external_id)
            genes.append({gene[0]: gene[1]})

        return genes

    def ask_for_melt_probes_ratio(self):
        """Calculates the ratio between the amount of
        unique melttemperatures and the amount of probes."""
        self.cursor.callproc("sp_get_tm_vs_probes")
        ratio = self.cursor.fetchone()
        return ratio

    def mark_duplicates(self):
        """Marks duplicate probes"""
        self.cursor.execute("select id, sequentie from oligonucleotiden;")
        out = self.cursor.fetchall()
        for oligo in out:  # oligo = (id, sequentie)
            self.cursor.execute("call sp_find_duplicates(" + oligo[1] + ", @dub)")
            dub = self.cursor.fetchone()
            if dub == 1:  # it's a duplicate
                self.cursor.execute("call sp_mark_duplicate_oligos(" + oligo[0] + ")")

        return True


# MAIN
def main():
    """Tests the class"""
    TalkToDatabase()
    # FINISH
    return 0

# RUN
if __name__ == '__main__':
    sys.exit(main())
