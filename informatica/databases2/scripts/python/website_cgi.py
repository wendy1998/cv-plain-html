#!/usr/bin/env python3
"""
CGI script that creates a webpage with names of students in a database.
 Usage: click link thema 6 on "bioinf.nl/~wbvandermeulen/index.html underneath 'themaopdracht -> jaar 2'"
 Output: Webpage with names of students in a database.
"""

# IMPORTS
import sys
import cgi
from database_management import TalkToDatabase

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "0.1"
__status__ = "CGI script"


# CLASSES
class WriteWebsite:
    """Class that writes a website"""
    def __init__(self, fieldstorage):
        self.fieldstorage = fieldstorage
        self.students = []
        self.results = []
        print('<div style="display: none">')  # error messages don't need to be shown on the webpage.
        if self.fieldstorage.getvalue("student"):  # exams of a specific student need to be fetched
            self.connection = TalkToDatabase("wbvandermeulen", "Wbvandermeulen",
                                             "tRn51Ha4", "mysql.bin",
                                             student=self.fieldstorage.getvalue("student"))
            self.results = self.connection.get_exams()
        else:  # names of all the students in the database need to be fetched.
            self.connection = TalkToDatabase("wbvandermeulen", "Wbvandermeulen",
                                             "tRn51Ha4",
                                             "mysql.bin")
            self.students = self.connection.get_students()
        print("</div>")
        self.content = self.make_webpage_content()
        self.write_webpage()

    def make_webpage_content(self):
        """Makes the content for the webpage written later"""
        content = []
        if self.students:  # first time writing the webpage, table with all the student names.
            content.append("<table>")
            counter = 1
            for student in self.students:
                if counter % 2 == 1:
                    content.append("<tr>")
                content.append('<td><a href="write_webpage.cgi?'
                               'student={0}">{0}</a></td>'
                               .format(student))
                if counter % 2 == 0:
                    content.append("</tr>")
                counter += 1

            # if there is no second entry in the last row of the table,
            # the table row will never be closed through the foregoing loop.
            if len(self.students) % 2 == 1:
                content.append("</tr>")
            content.append("</table>")
        elif self.results:  # a student was selected and the exam results need to be the content of the webpage
            for result in self.results:  # result = tentamen object with student, course, date and mark
                if result.get_student() == "NULL":
                    content.append("This student hasn't made any tests.")
                else:
                    content.append("vak:{0}{1}{2}datum:{0}{3}{2}cijfer:{0}{4}{2}{2}".format("\t", result.get_vak(), "<br/>",
                                                                                            result.get_datum(),
                                                                                            result.get_cijfer()))

        else:  # something went wrong with connecting to the database from the first form and the first form needs to be filled again.
            content.append("Helaas is er iets fout gegaan met het verbinden naar de database, "
                           "kom alstublieft later terug.")
        return content

    def write_webpage(self):
        """Writes a webpage in a certain way."""
        with open("../../template.html") as template:
            for line in template:
                if line.strip() != "<!---inhoud--->":
                    print(line)
                else:
                    for string in self.content:
                        print(string)
        return 0


# MAIN
def main():
    """Takes the program through it's functions"""
    # PREPARATIONS
    print("Content-type: Text/html\n\n")
    inp = cgi.FieldStorage()

    # WORK
    WriteWebsite(inp)

    # FINISH
    return 0


if __name__ == '__main__':
    sys.exit(main())
