#!/usr/bin/env python3
"""
    This module has a class which "talks" to the "tentamen" part of the database the program works with.
    Usage: import tentamen
"""

# IMPORTS
import sys
import time
import datetime
import dateutil.parser as parser

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "0.1"
__status__ = "Module"


# CLASSES
# errors
class MarkTooLowError(Exception):
    """Error for a mark that's too low"""
    pass


class MarkTooHighError(Exception):
    """Error for a mark that's too high"""
    pass


class MarkForFutureTest(Exception):
    """Error that's used for notifying about a mark for a future test"""
    pass


class UnknownDateError(Exception):
    """Error that's used for notifying about an unknown date, that will be converted to a standard date"""
    pass


class Tentamen:
    """Class that resembles an exam."""
    def __init__(self, student, vak, datum, cijfer=0.0):
        self.student = self.set_student(student)
        self.vak = self.set_vak(vak)
        self.datum = self.set_datum(datum)
        self.cijfer = self.set_cijfer(cijfer)

    # setters
    def set_student(self, student):
        """Sets the student variable"""
        return student

    def set_vak(self, vak):
        """Sets the vak variable"""
        return vak

    def set_datum(self, datum):
        """Sets the datum variable"""
        if datum == "NULL":
            try:
                raise UnknownDateError
            except UnknownDateError:
                print("Unknown date: date converted to 1111-11-11 for the benefit of the program.")

            return "1111-11-11"
        else:
            return str(parser.parse(datum, dayfirst=True))[:10]

    def set_cijfer(self, cijfer):
        """Sets the cijfer variable after checking if it's correct."""
        if datetime.datetime.strptime(self.datum, "%Y-%m-%d").timestamp() > \
                datetime.datetime.strptime(time.strftime("%d-%m-%Y"), "%d-%m-%Y").timestamp() and not cijfer == 0.0:
            try:
                raise MarkForFutureTest
            except MarkForFutureTest:
                print("Mark present for exam with future date, mark reversed to Null, "
                      "because test couldn't have been made yet.")
            return "NULL"
        elif datetime.datetime.strptime(self.datum, "%Y-%m-%d").timestamp() > \
                datetime.datetime.strptime(time.strftime("%d-%m-%Y"), "%d-%m-%Y").timestamp():
            return "NULL"
        elif cijfer == 0.0:
            return "NULL"
        else:
            try:
                if cijfer < 1:
                    raise MarkTooLowError
                elif cijfer > 10:
                    raise MarkTooHighError
            except MarkTooLowError:
                print("This mark is too low. Marks have to be at least 1.0.")
                return "NULL"
            except MarkTooHighError:
                print("This mark is too high. Marks have to be at most 10.0.")
                return "NULL"
            return cijfer

    # getters
    def get_student(self):
        """gets the student variable"""
        return self.student

    def get_vak(self):
        """gets the vak variable"""
        return self.vak

    def get_datum(self):
        """gets the datum variable"""
        if self.datum == "1111-11-11":
            return "Date unknown"
        else:
            return self.datum

    def get_cijfer(self):
        """gets the cijfer variable if there is one"""
        if datetime.datetime.strptime(self.datum, "%Y-%m-%d").timestamp() > \
                datetime.datetime.strptime(time.strftime("%d-%m-%Y"), "%d-%m-%Y").timestamp():
            return "This exam wasn't made yet, there is no mark at the time."
        else:
            return self.cijfer

    def __str__(self):
        """Represents the class in a string format"""
        return "student:{0}{1}{2}vak:{0}{3}{2}datum:{0}{4}{2}cijfer:{0}{5}"\
            .format("\t", self.student, "\n", self.vak, self.datum, self.cijfer)


# DEFINITIONS
def check_string_representation():
    """Checks wether the string representation of the class works correctly"""
    tentamen1 = Tentamen("Wendy van der Meulen", "Databases1", "10-11-2017", 10)
    if tentamen1.__str__() != "student:\tWendy van der Meulen\n" \
                            "vak:\tDatabases1\n" \
                            "datum:\t2017-11-10\n" \
                            "cijfer:\t10":
        print("Not the expected string output!")
    else:
        print("String output is right!")

    return 0


def check_future_dates():
    """Checks wether the marks behaviour is right when the test isn't made yet."""
    tentamen2 = Tentamen("Wendy van der Meulen", "Databases2", "10-02-2018")
    if tentamen2.get_cijfer() != "This exam wasn't made yet, there is no mark at the time.":
        print("Not the expected mark output for future date!")
    else:
        print("Future date mark behaviour is correct!")
    return 0


def check_too_low_mark():
    """Checks wether the marks behaviour is right when the test has a mark that's too low."""
    tentamen3 = Tentamen("Wendy van der Meulen", "Databases1", "10-11-2017", 0.1)
    if tentamen3.get_cijfer() != "Null":
        print("Not the expected mark output for too low a mark!")
    else:
        print("too low mark behaviour is correct!")
    return 0


def check_too_high_mark():
    """Checks wether the marks behaviour is right when the test has a mark that's too high."""
    tentamen4 = Tentamen("Wendy van der Meulen", "Databases1", "10-11-2017", 11)
    if tentamen4.get_cijfer() != "Null":
        print("Not the expected mark output for too high a mark!")
    else:
        print("too high mark behaviour is correct!")
    return 0


def check_good_mark():
    """Checks wether the marks behaviour is right when the test has a mark that's accepted."""
    tentamen5 = Tentamen("Wendy van der Meulen", "Databases1", "10-11-2017", 10)
    if tentamen5.get_cijfer() != 10:
        print("Not the expected mark output for a normal mark!")
    else:
        print("normal mark behaviour is correct!")
    return 0


def check_past_date():
    """Checks wether the marks behaviour is right when the test has a mark."""
    check_too_low_mark()
    check_too_high_mark()
    check_good_mark()


def check_mark_behaviour():
    """Checks if all the tests in association with the mark of an exam work right"""
    check_future_dates()
    check_past_date()


# MAIN
def main():
    """Tests class Tentamen"""
    check_string_representation()
    check_mark_behaviour()
    return 0

if __name__ == '__main__':
    sys.exit(main())
