
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'label');
                data.addColumn('number', 'percentage');
                data.addRows([["Bruine Oogkleur, Blonde Haarkleur, Lange Lichaamslengte, Goede zicht", 31.640625],
["Bruine Oogkleur, Bruine Haarkleur, Kleine Lichaamslengte, Goede zicht", 3.515625],
["Blauwe Oogkleur, Blonde Haarkleur, Kleine Lichaamslengte, Slechte zicht", 1.171875],
["Bruine Oogkleur, Blonde Haarkleur, Kleine Lichaamslengte, Slechte zicht", 3.515625],
["Bruine Oogkleur, Bruine Haarkleur, Lange Lichaamslengte, Slechte zicht", 3.515625],
["Blauwe Oogkleur, Bruine Haarkleur, Kleine Lichaamslengte, Slechte zicht", 0.390625],
["Bruine Oogkleur, Blonde Haarkleur, Kleine Lichaamslengte, Goede zicht", 10.546875],
["Blauwe Oogkleur, Blonde Haarkleur, Kleine Lichaamslengte, Goede zicht", 3.515625],
["Bruine Oogkleur, Bruine Haarkleur, Lange Lichaamslengte, Goede zicht", 10.546875],
["Bruine Oogkleur, Blonde Haarkleur, Lange Lichaamslengte, Slechte zicht", 10.546875],
["Blauwe Oogkleur, Bruine Haarkleur, Lange Lichaamslengte, Goede zicht", 3.515625],
["Blauwe Oogkleur, Bruine Haarkleur, Kleine Lichaamslengte, Goede zicht", 1.171875],
["Blauwe Oogkleur, Bruine Haarkleur, Lange Lichaamslengte, Slechte zicht", 1.171875],
["Bruine Oogkleur, Bruine Haarkleur, Kleine Lichaamslengte, Slechte zicht", 1.171875],
["Blauwe Oogkleur, Blonde Haarkleur, Lange Lichaamslengte, Slechte zicht", 3.515625],
["Blauwe Oogkleur, Blonde Haarkleur, Lange Lichaamslengte, Goede zicht", 10.546875],
]);

                      var options = {
                        title: 'Mogelijke fenotypen* voor het kind:',
                        sliceVisibilityThreshold: .01
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart_f'));
                      chart.draw(data, options);
                    }
                