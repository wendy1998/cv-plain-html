#!/usr/bin/env python3
"""
Module containing the properties of the person object, it's subclasses and their relations.
"""

# IMPORTS
import re

# METADATA
__author__ = "Niels van der vegt & Wendy van der Meulen"  # Every function was written by Wendy, unless otherwise specified.
__version__ = "1.0"


# CLASSES
class Person:
    """
    Superclass for every person
    """
    def sort_string(self, string):
        """
        Sorts strings alphabetically giving capitals of each letter priority over their
        lower case variant. For example: a given string, 'ABab', will be sorted as 'AaBb'.

        --- Returns ---
        sorted_string (string) : Alphabetically sorted string, giving priority to upper
        case letters.

        This function was written by Niels van der Vegt.
        """
        # The key argument in sorted is given a lambda. This lambda checks if the first
        # letter of a string is a lower case letter and returns an appropriate boolean.
        # False booleans are placed before True booleans, so capital letters of the same
        # letter will be placed first.
        sorted_string = "".join(sorted(string, key=lambda x: (x.upper(), x[0].islower())))

        return sorted_string


class Parent(Person):
    """
    Class that simulates a parent with its genetic information.
    """
    def __init__(self, genotype):
        self.genotype = self.sort_string(genotype)
        self.alleles = self.get_alleles()
        self.gametes = self.get_gametes()

    def get_alleles(self):
        """
        Converts the input genotype into a list of groups of two alleles.

        --- Returns ---
        alleles (list) :
        """
        alleles = re.findall("..", self.genotype)

        return alleles

    def get_gametes(self):
        """
        Calculates the Cartesian product of a list of all traits/alleles to form all
        possible gametes of the current genotype.

        --- Returns ---
        gametes (set) : set of all possible gametes of the object's genotype.

        This function was written by Niels van der Vegt.
        """
        gametes = [[]]

        # Calculates all possible combinations of alleles within the genotype.
        for trait in self.alleles:
            gametes = [gamete+[allele] for gamete in gametes for allele in trait]

        # Converts the list objects to sorted strings and uses set to remove duplicate gametes.
        return set(["".join(self.sort_string(gamete)) for gamete in gametes])

    def __str__(self):
        return "My genotype is: {0},{2}My alleles are: {1}{2}And my possible gametes are: {3}."\
            .format(self.genotype, self.alleles, "\n", self.gametes)


class Child(Person):
    """
    Class that simulates a child with it's parents genetic information.
    """
    def __init__(self, father, mother):
        self.possible_genotypes = self.get_possible_genotypes(father.gametes, mother.gametes)
        self.possible_fenotypes = self.get_possible_fenotypes()
        self.percentage_genotype = self.calculate_percentage_dict(self.possible_genotypes)
        self.percentage_fenotype = self.calculate_percentage_dict(self.possible_fenotypes)

    def get_possible_genotypes(self, father_gametes, mother_gametes):
        """
        Calculates the possible genotypes for a child out of the given parent gametes.
        """
        genotypes = []
        for f_gamete in father_gametes:
            for m_gamete in mother_gametes:
                genotypes.append("".join([f_gamete, m_gamete]))

        sorted_genotypes = [self.sort_string(genotype) for genotype in genotypes]

        return sorted_genotypes

    def get_possible_fenotypes(self):
        """
        Gets the possible fenotypes
        """
        fenotypes = []
        for genotype in self.possible_genotypes:
            traits = re.findall("..", genotype)
            fenotype = []
            for trait in traits:
                # trait is sorted with capital first, so if the first one isn't capital
                # then the whole trait is recessive, which means you can append the first character
                # either way for the fenotype.
                fenotype.append(trait[0])
            fenotypes.append("".join(fenotype))
        return list(fenotypes)

    def calculate_percentage_dict(self, iterable):
        """
        Calculates the percentage of items in an iterable.

        --- Return ---
        percentage dict: The percentage that the given data is represented in the iterable
        """
        percentage_dict = {}
        for item in iterable:
            percentage_dict[item] = (iterable.count(item) / len(iterable)) * 100

        return percentage_dict

    def __str__(self):
        return "My possible genotypes are: {0},{2}My possible fenotypes are: {1}, " \
               "{2}The genotype possibilies have these percentages: {3},{2}" \
               "And The fenotype possibilies have these percentages: {4}."\
            .format(list(set(self.possible_genotypes)), list(set(self.possible_fenotypes)), "\n",
                    self.percentage_genotype, self.percentage_fenotype)
