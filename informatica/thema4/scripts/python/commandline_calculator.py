#!/usr/bin/env python3
"""
This program gets the genotypes of two parents and calculates the chances of the appearance of the
offspring, would these two parents mate.

    Usage: ./commandline_calculator.py -f <genotype father> -m <genotype mother>
    Output: the chances of the appearance of the offspring on the commandline
"""

# IMPORTS
import sys
import argparse
import re
from collections import Counter
import progeny_calculator
import errors

# METADATA
__author__ = "Wendy van der Meulen & Niels van der Vegt"
__version__ = "1.0"


# CLASSES
class CommandlineCalculator(progeny_calculator.ProgenyCalculator):
    """
    Progeny calculator for the commandline interface
    """
    def __init__(self):
        # PREPARATIONS
        self.cml_args = self.parse_command_line()
        self.genotype_father = self.cml_args.genotype_father
        self.genotype_mother = self.cml_args.genotype_mother

        # WORK
        self.validate_genotype(self.cml_args.genotype_father,
                               self.cml_args.genotype_mother)
        progeny_calculator.ProgenyCalculator.__init__(self, father=self.genotype_father,
                                                      mother=self.genotype_mother)

        # FINISH
        self.print_results()

    def parse_command_line(self):
        """
        Parses commandline arguments
        """
        parser = argparse.ArgumentParser(description="Genetic Punnett Calculator")

        parser.add_argument("-f", "--genotype_father", help="Insert the father genotype", required=True)
        parser.add_argument("-m", "--genotype_mother", help="Insert the mother genotype", required=True)

        # Check if all necessary arguments have been given
        if len(sys.argv) != 5:
            parser.print_help()
            sys.exit(1)

        cml_args = parser.parse_args()

        return cml_args

    def validate_genotype(self, genotype_father, genotype_mother):
        """
        Checks if the provided genotype is valid.

        --- Parameters ---
        genotypes
        """
        for genotype in [genotype_father, genotype_mother]:
            try:
                if len(genotype_father) != len(genotype_mother):
                    raise errors.GenotypeLengthError
                # Raises InvalidCharacterError if any characters are not in the alphabet.
                if not re.fullmatch("[a-zA-Z]+", genotype):
                    raise errors.InvalidCharacterError
                # Raises InvalidAllelesError if any trait is shorter or longer than two alleles.
                for value in Counter(genotype.lower()).values():
                    if value != 2:
                        raise errors.InvalidAllelesError

            except errors.GenotypeLengthError:
                print("Error: Invalid Genotype length - "
                      "Make sure that the parent genotypes are of the same length.")
                sys.exit(1)

            except errors.InvalidCharacterError:
                print("Error: Invalid Character(s) - "
                      "Make sure there are no numbers or symbols present in the genotype.")
                sys.exit(1)

            except errors.InvalidAllelesError:
                print("Error: Invalid Allele(s) - "
                      "Make sure there are exactly 2 dominant or recessive alleles per trait.")
                sys.exit(1)


# MAIN
def main():
    """
    takes the program through it's functions
    """
    # WORK
    CommandlineCalculator()
    # FINISH
    return 0

# ENTRY POINT
if __name__ == "__main__":
    sys.exit(main())
