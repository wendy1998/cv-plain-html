#!/usr/bin/env python3
"""
The superclass for a commandline calculator and a web calculator,
"""

import person

# METADATA
__author__ = "Niels van der vegt & Wendy van der Meulen"
__version__ = "1.0"


# CLASSES
class ProgenyCalculator:
    """
    A progeny calculator superclass
    """
    def __init__(self, father, mother, web=False):
        self.father = person.Parent(father)
        self.mother = person.Parent(mother)
        self.child = person.Child(self.father, self.mother)
        self.web = web

    def print_results(self):
        """
        Prints the results of the calculation.
        """
        self.print_gametes()
        print("The possible genotypes for F1 are:")
        self.print_percentage_dict(self.child.percentage_genotype)
        print("The possible fenotypes for F1 are:")
        self.print_percentage_dict(self.child.percentage_fenotype)
        return 0

    def print_gametes(self):
        """
        Prints the gametes of the parents.
        """
        if self.web:
            print("<p>De mogelijke gameten* voor de vader zijn:")
        else:
            print("The possible gametes for the father are:")
        print(", ".join(self.father.gametes), end="\n")
        if self.web:
            print("<br/>De mogelijke gameten* voor de moeder zijn:")
        else:
            print("The possible gametes for the mother are:")
        print(", ".join(self.mother.gametes), end="\n")
        if self.web:
            print("</p>")
        return 0

    def print_percentage_dict(self, dictionary):
        """
        Prints the keys with their values, sorted on their values in reverse.
        """
        for key, value in sorted(dictionary.items(), key=lambda x: x[1], reverse=True):
            print(key + ": " + str(round(value, 2)) + "%", end="\n")

        return 0
