#!/usr/bin/env python3
""" Program that processes the data it gets from the web and returns an appropriate HTML page. """

# IMPORTS
import cgi
import re
from collections import Counter
import sys

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "1.0"


# DEFINITIONS
def sort_genotype(genotype):
    """
    Sorts a genotype to make sure that all the alleles of a trait are next to each other.
    """
    sorted_genotype = "".join(sorted(genotype, key=lambda x: (x.upper(), x[0].islower())))
    return sorted_genotype


def get_alleles(genotype):
    """
    Converts the input genotype into a list of groups of two alleles.
    """
    genotype = re.findall("..", genotype)
    return genotype


def not_valid_genotype(genotype):
    """
    Checks if the provided genotype is valid.

    --- Parameters ---
    genotype
    """
    if not re.fullmatch("[a-zA-Z]+", genotype):
        return True
    for value in Counter(genotype.lower()).values():
        if value != 2:
            return True

    return False


def print_page(mother, father, way="0"):
    """
    Prints the output HTML page in a certain way and exits the script afterwards.
    """
    print('''Content-type: text/html\n\n
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
          <head>
            <link rel="stylesheet" type="text/css" href="../../css/style.css"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
            <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
            <script src="../javascript/script.js" type="text/javascript"></script>
            <title>Bio-calculator</title>
          </head>
          <body>
            <div class="topcontainer">
              <h1><a href="../../thema4_index.html">BioGen</a></h1>
              <h3><i>Alles over biologie en genetica</i></h3>
              <div id="cssmenu">
                <ul>
                      <li>
                          <a href="../../thema4_index.html">Home</a>
                      </li>
                      <li class ="active">
                          <a href="../../bio_calculator.html">Bio-calculator</a>
                      </li>
                      <li>
                          <a href="../../werking.html">Werking</a>
                      </li>
                      <li class="last">
                          <a href="../../mendel.html">Mendel</a>
                      </li>
                        </ul>
              </div>
              <div id="dropdownmenu">
                <ul>
                  <li class="has-sub">
                      <a href="#">
                          <img src="../../images/drop_menu.png" alt=""/>
                          </a>
                    <ul>
                          <li>
                            <a href="../../thema4_index.html">Home</a>
                          </li>
                          <li class = "active">
                            <a href="../../bio_calculator.html">Bio-calculator</a>
                          </li>
                          <li>
                              <a href="../../werking.html">Werking</a>
                          </li>
                          <li>
                              <a href="../../mendel.html">Mendel</a>
                          </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="container">
              <p>Bereken hier wat de mogelijke genotypen*
              en fenotypen* kunnen zijn voor een kind.<br/>
		      Voor wat er precies ingevuld moet worden, zie de pagina "werking".</p>
    ''')

    if way == "onvolledig":
        print('''
              <p class="red">Vul alsjeblieft alle gegevens in.</p>
              <form id="form" action="verwerking_1.cgi" method="post">
                <div>
                  <label>Genotype* moeder: (AaBbCC enz.)<br/>
                      <input type="text" name= "mother" class="invulform" value="Genotype moeder"/></label><br/>
                  <label>Genotype* vader: (AaBbCC enz.)<br/>
                      <input type="text" name="father" class="invulform" value="Genotype vader"/></label><br/>
                  <input id="submit" type="submit" value="Submit" name="submit"/>
                </div>
              </form>
    ''')
    elif way == "alleles":
        print('''
              <p class="red">Vul genotypen* met dezelfde allelen* in.</p>
              <form id="form" action="verwerking_1.cgi" method="post">
                <div>
                  <label>Genotype* moeder: (AaBbCC enz.)<br/>
                      <input type="text" name= "mother" class="invulform" value="Genotype moeder"/></label><br/>
                  <label>Genotype* vader: (AaBbCC enz.)<br/>
                      <input type="text" name="father" class="invulform" value="Genotype vader"/></label><br/>
                  <input id="submit" type="submit" value="Submit" name="submit"/>
                </div>
              </form>
            ''')
    elif way == "not_same_length":
        print('''
              <p class="red">Vul genotypen* met dezelfde lengte in.</p>
              <form id="form" action="verwerking_1.cgi" method="post">
                <div>
                  <label>Genotype* moeder: (AaBbCC enz.)<br/>
                      <input type="text" name= "mother" class="invulform" value="Genotype moeder"/></label><br/>
                  <label>Genotype* vader: (AaBbCC enz.)<br/>
                      <input type="text" name="father" class="invulform" value="Genotype vader"/></label><br/>
                  <input id="submit" type="submit" value="Submit" name="submit"/>
                </div>
              </form>
            ''')
    elif way == "invalid":
        print('''
              <p class="red">Dit genotype* is invalid, vul een nieuwe in.<br/>
              voor de eisen, zie "werking".</p>
              <form id="form" action="verwerking_1.cgi" method="post">
                <div>
                  <label>Genotype* moeder: (AaBbCC enz.)<br/>
                      <input type="text" name= "mother" class="invulform" value="Genotype moeder"/></label><br/>
                  <label>Genotype* vader: (AaBbCC enz.)<br/>
                      <input type="text" name="father" class="invulform" value="Genotype vader"/></label><br/>
                  <input id="submit" type="submit" value="Submit" name="submit"/>
                </div>
              </form>
            ''')
    else:
        print('''
          <form id="form" action="web_calculator.cgi" method="get">
            <div>
              <fieldset class="invisible">
                <legend>Parents:</legend>
                  <label>Genotype moeder:<br/>
                  <input type="text" name="mother" class="invulform"
                  value="''' + mother + '''"/></label><br/>
                  <label>Genotype vader:<br/>
                  <input type="text" name="father" class="invulform"
                  value="''' + father + '''"/></label><br/>
              </fieldset>
        ''')
        for count, trait in enumerate(get_alleles(mother)):
            print('''
                  <fieldset>
                    <legend>Eigenschap ''' + trait[0].upper() + ''':</legend>
                      <label>Waar staat de eigenschap''', trait[0].upper(), '''in het algemeen voor?<br/>
                      <input type="text" name="trait_''' + str(count+1) + '''" class="invulform"/></label><br/>
                      <label>Waar staat het dominante* allel* voor?<br/>
                      <input type="text" name="dominance_''' + str(count+1) + '''" class="invulform"/></label><br/>
                      <label>Waar staat het recessieve* allel* voor?<br/>
                      <input type="text" name="recessive_''' + str(count+1) + '''" class="invulform"/></label><br/>
                      <label>Is het een ziekte?<br/>
                      <select name="sickness_''' + str(count+1) + '''">
                        <option value="yes">Ja</option>
                        <option value="no" selected>Nee</option>
                      </select></label><br/>
                  </fieldset>''')

        print('''
                  <input id="submit" type="submit" value="Submit" name="submit"/>
                </div>
              </form>''')

    print('''
         <p>*Deze begrippen worden uitgelegd op de pagina "mendel"</p>
        </div>
      </body>
    </html>
    ''')
    sys.exit()


# MAIN
def main():
    """
    Takes the program through it's functions.
    """
    # PREPARATIONS
    genotypes = cgi.FieldStorage()
    if len(genotypes) == 0:
        print("Sorry, this program can only be run from the web.")
        sys.exit(1)

    mother = genotypes.getvalue("mother")
    father = genotypes.getvalue("father")

    # WORK
    if mother == "Genotype moeder" and father == "Genotype vader" or \
            (mother == "Genotype moeder" or father == "Genotype vader"):
        print_page(mother, father, "onvolledig")

    mother = sort_genotype(mother)
    father = sort_genotype(father)

    if len(father) != len(mother):
        print_page(mother, father, "not_same_length")
    elif not_valid_genotype(father) or not_valid_genotype(mother):
        print_page(mother, father, "invalid")
    elif set(mother.upper()) != set(father.upper()):
        print_page(mother, father, "alleles")

    # FINISH
    print_page(mother, father)

# RUN
if __name__ == "__main__":
    main()
