#!/usr/bin/env python3
"""
This program gets the genotypes of two parents and calculates the chances of the appearance of the
offspring, would these two parents mate.

    Usage: ./web_calculator.cgi -f <genotype father> -m <genotype mother> or
        fill in the bio-calculator form online.
    Output: the chances of the appearance of the offspring on the commandline or
        on a webpage, with a piechart representing these.
"""

# IMPORTS
import cgi
import sys
import re
import progeny_calculator
import commandline_calculator

# METADATA
__author__ = "Wendy van der Meulen"
__version__ = "1.0"


# CLASSES
class WebCalculator(progeny_calculator.ProgenyCalculator):
    """
    A progeny calculator that works with input from a webform and prints a webpage.
    """
    def __init__(self):
        # PREPARATIONS
        self.web_args = cgi.FieldStorage()
        self.genotype_father = self.web_args.getvalue("father")
        self.genotype_mother = self.web_args.getvalue("mother")

        # WORK
        progeny_calculator.ProgenyCalculator.__init__(self, father=self.genotype_father,
                                                      mother=self.genotype_mother,
                                                      web=True)

        # FINISH
        # there are two non-trait related elements in web_args, so that's why -2.
        # and for every trait, there are four things in web_args:
        # trait name, dominant allele, recessive allele and if it's a sickness,
        # so that's why /4.
        if int((len(self.web_args) - 2) / 4) != len(set(self.genotype_mother.upper())):
            self.print_html_page("incomplete")
        self.check_traits()
        self.print_html_page()

    def check_traits(self):
        """
        Checks if the given input is valid.
        """
        # there are two non-trait related elements in web_args, so that's why -2.
        # and for every trait, there are four things in web_args:
        # trait name, dominant allele, recessive allele and if it's a sickness,
        # so that's why /4.
        for i in range(int((len(self.web_args) - 2) / 4)):
            trait = "trait_" + str(i + 1)  # trait_1, trait_2 enz.
            dominance = "dominance_" + str(i + 1)  # dominance_1, dominance_2 enz.
            recessive = "recessive_" + str(i + 1)  # recessive_1, recessive_2 enz.
            characters = [self.web_args.getvalue(trait),
                          self.web_args.getvalue(dominance),
                          self.web_args.getvalue(recessive)]
            for character in characters:
                if not re.fullmatch("[a-zA-Z0-9-]+", character):
                    self.print_html_page("invalid")
        return 0

    def get_labels_fenotypes(self):
        """
        Makes the onderstandable labels for the fenotypes piechart.

        ---return---
        A dictionary with the fenotype as key and the understandable label as value.  # AB: "blonde haarkleur, Bruine ogen"
        """
        labels_dict = {}
        no_e = ["wel", "niet", "geen", "goed", "slecht"]
        not_sick = ["niet ziek", "gezond", "heeft het niet", "heeft de ziekte niet"]
        sick = ["ziek", "heeft het", "heeft de ziekte"]
        for fenotype in self.child.possible_fenotypes:
            label = []
            # there are two non-trait related elements in web_args, so that's why -2.
            # and for every trait, there are four things in web_args:
            # trait name, dominant allele, recessive allele and if it's a sickness,
            # so that's why /4.
            for i in range(int((len(self.web_args) - 2) / 4)):
                trait = "trait_" + str(i + 1)  # trait_1, trait_2 enz.
                dominance = "dominance_" + str(i + 1)  # dominance_1, dominance_2 enz.
                recessive = "recessive_" + str(i + 1)  # recessive_1, recessive_2 enz.
                sickness = "sickness_" + str(i + 1)  # sickness_1, sickness_2 enz.
                # if the character is lower, it's the recessive fenotype.
                if fenotype[i].islower():
                    allele = recessive
                # dominant fenotype
                else:
                    allele = dominance
                # write label
                if self.web_args.getvalue(sickness) == "yes":  # it's a sickness
                    if self.web_args.getvalue(allele).lower() in sick:
                        label.append(" ".join(["Heeft", self.web_args.getvalue(trait)]))
                    elif self.web_args.getvalue(allele).lower() in not_sick:
                        label.append(" ".join(["Heeft geen", self.web_args.getvalue(trait)]))
                    else:
                        label.append(" ".join(["Onbekend of het kind", self.web_args.getvalue(trait), "heeft."]))
                # checks if there has to be put an "e" behind the allele to make the label easy to read. -> blond!e! haarkleur
                elif not self.web_args.getvalue(allele).endswith("e") and \
                        self.web_args.getvalue(allele) not in no_e:
                        # checks if the value is a character, and otherwise turns value and key around.
                    if not self.web_args.getvalue(allele).isalpha():
                        label.append(" ".join([self.web_args.getvalue(trait), self.web_args.getvalue(allele)]))
                    else:
                        label.append(" ".join(["".join([self.web_args.getvalue(allele), "e"]),
                                               self.web_args.getvalue(trait)]))
                else:
                    label.append(" ".join([self.web_args.getvalue(allele), self.web_args.getvalue(trait)]))
            labels_dict[fenotype] = ", ".join(label)  # AB: "blonde haarkleur, bruine oogkleur"
        return labels_dict

    def fuse_labels_and_percentages(self):
        """
        Fuses the labels in the labels dict and the percentages in the percentage dict into a list.

        ---returns---
        A dictionary with the fenotype as key and a list with the label and the percentage as value.
        AB: ["blonde haarkleur, bruine oogkleur", 6.25%]
        """
        fenotype_information_dict = {}
        fenotype_labels = self.get_labels_fenotypes()
        # [(AB, "blonde haarkleur, bruine oogkleur"), (ab, "bruine haarkleur, blauwe oogkleur")]
        for key_label, value_label in zip(fenotype_labels, fenotype_labels.values()):
            # [(AB, 6.25%), (ab, 6.25%)]
            for key_per, value_per in zip(self.child.percentage_fenotype,
                                          self.child.percentage_fenotype.values()):
                if key_label == key_per:
                    # {AB: ["blonde haarkleur, bruine oogkleur", 6.25%],
                    # ab: ["bruine haarkleur, blauwe oogkleur", 6.25%]}
                    fenotype_information_dict[key_label] = [value_label, value_per]

        return fenotype_information_dict

    def write_piechart_scripts(self):
        """
        Writes the two scripts that visualize the piecharts in the webpage.
        """
        f_script = "../javascript/pie_f.js"
        g_script = "../javascript/pie_g.js"
        for script in [f_script, g_script]:
            with open(script, "w") as open_script:
                open_script.write('''
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'label');
                data.addColumn('number', 'percentage');
                data.addRows([''')
                if "f" in script:  # fenotype piechart
                    # ["blonde haarkleur, bruine oogkleur", 6.25%]
                    for info in self.fuse_labels_and_percentages().values():
                        open_script.write("[")
                        open_script.write('"' + info[0] + '"' + ", ")
                        open_script.write(str(float(info[1])))
                        open_script.write("]")
                        open_script.write(",\n")
                        # ["blonde haarkleur, bruine oogkleur, 6.25%]\n"
                else:  # genotype piechart
                    # [(AaBb, 6.25%), (aabb, 6.25%)]
                    for genotype, perc in zip(self.child.percentage_genotype,
                                              self.child.percentage_genotype.values()):
                        open_script.write("[")
                        open_script.write('"' + genotype + '"' + ", ")
                        open_script.write(str(float(perc)))
                        open_script.write("]")
                        open_script.write(",\n")
                        # [AaBb, 6.25%]\n"
                open_script.write(''']);

                      var options = {
                        title: 'Mogelijke ''')
                if "f" in script:  # fenotype piechart
                    open_script.write("fenotypen*")  # Mogelijke fenotypen voor een kind
                else:  # genotype piechart
                    open_script.write("genotypen*")  # Mogelijke genotypen voor een kind

                open_script.write(''' voor het kind:',
                        sliceVisibilityThreshold: .01
                      };

                      var chart = new google.visualization.PieChart(document.getElementById(''')
                if "f" in script:
                    open_script.write("'piechart_f'")  # fenotype piechart
                else:
                    open_script.write("'piechart_g'")  # genotype piechart
                open_script.write('''));
                      chart.draw(data, options);
                    }
                ''')
        return 0

    def print_html_page(self, way="0"):
        """
        Prints the output HTML page in a certain way and exits the script afterwards.
        """
        print('''Content-type: text/html\n\n
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                  <head>
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                    <script type="text/javascript" src="../javascript/pie_f.js"></script>
                    <script type="text/javascript" src="../javascript/pie_g.js"></script>
                    <link rel="stylesheet" type="text/css" href="../../css/style.css"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
                    <script src="../javascript/script.js" type="text/javascript"></script>
                    <title>Bio-calculator</title>
                  </head>
                  <body>
                    <div class="topcontainer">
                      <h1><a href="../../thema4_index.html">BioGen</a></h1>
                      <h3><i>Alles over biologie en genetica</i></h3>
                      <div id="cssmenu">
                        <ul>
                              <li>
                                  <a href="../../thema4_index.html">Home</a>
                              </li>
                              <li class ="active">
                                  <a href="../../bio_calculator.html">Bio-calculator</a>
                              </li>
                              <li>
                                  <a href="../../werking.html">Werking</a>
                              </li>
                              <li class="last">
                                  <a href="../../mendel.html">Mendel</a>
                              </li>
                                </ul>
                      </div>
                      <div id="dropdownmenu">
                        <ul>
                          <li class="has-sub">
                              <a href="#">
                                  <img src="../../images/drop_menu.png" alt=""/>
                                  </a>
                            <ul>
                                  <li>
                                    <a href="../../thema4_index.html">Home</a>
                                  </li>
                                  <li class ="active">
                                    <a href="../../bio_calculator.html">Bio-calculator</a>
                                  </li>
                                  <li>
                                      <a href="../../werking.html">Werking</a>
                                  </li>
                                  <li>
                                      <a href="../../mendel.html">Mendel</a>
                                  </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="container">
                      <p>Bereken hier wat de mogelijke genotypen*
                      en fenotypen* kunnen zijn voor een kind.<br/>
                      Voor wat er precies ingevuld moet worden, zie de pagina "werking".</p>
            ''')
        if way == "incomplete":
            print('''
                      <p class="red">Vul alsjeblieft alles in.</p>
                      <form id="form" action="web_calculator.cgi" method="get">
                        <div>
                          <fieldset class="invisible">
                            <legend>Parents:</legend>
                              <label>Genotype moeder:<br/>
                              <input type="text" name="mother" class="invulform"
                              value="''' + self.mother.genotype + '''"/></label><br/>
                              <label>Genotype vader:<br/>
                              <input type="text" name="father" class="invulform"
                              value="''' + self.father.genotype + '''"/></label><br/>
                          </fieldset>
                ''')

            for count, trait in enumerate(self.mother.alleles):
                # Eigenschap A, trait_1, dominance_1, recessive_1, sickness_1; Eigenschap B, trait_2 ...
                print('''
                          <fieldset>
                            <legend>Eigenschap ''' + trait[0].upper() + ''':</legend>
                              <label>Waar staat de eigenschap''', trait[0].upper(), '''in het algemeen voor?<br/>
                              <input type="text" name="trait_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                              <label>Waar staat het dominante* allel* voor?<br/>
                              <input type="text" name="dominance_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                              <label>Waar staat het recessieve* allel* voor?<br/>
                              <input type="text" name="recessive_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                              <label>Is het een ziekte?<br/>
                              <select name="sickness_''' + str(count+1) + '''">
                                <option value="yes">Ja</option>
                                <option value="no">Nee</option>
                              </select></label><br/>
                          </fieldset>''')

            print('''
                          <input id="submit" type="submit" value="Submit" name="submit"/>
                        </div>
                      </form>''')
        elif way == "invalid":
            print('''
                      <p class="red">Vul alsjeblief alleen maar letters, cijfers of een koppelstreepje in.</p>
                      <form id="form" action="web_calculator.cgi" method="get">
                        <div>
                          <fieldset class="invisible">
                            <legend>Parents:</legend>
                              <label>Genotype moeder:<br/>
                              <input type="text" name="mother" class="invulform"
                              value="''' + self.mother.genotype + '''"/></label><br/>
                              <label>Genotype vader:<br/>
                              <input type="text" name="father" class="invulform"
                              value="''' + self.father.genotype + '''"/></label><br/>
                          </fieldset>
                ''')

            for count, trait in enumerate(self.mother.alleles):
                # Eigenschap A, trait_1, dominance_1, recessive_1, sickness_1; Eigenschap B, trait_2 ...
                print('''
                          <fieldset>
                            <legend>Eigenschap ''' + trait[0].upper() + ''':</legend>
                              <label>Waar staat de eigenschap''', trait[0].upper(), '''in het algemeen voor?<br/>
                              <input type="text" name="trait_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                              <label>Waar staat het dominante* allel* voor?<br/>
                              <input type="text" name="dominance_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                              <label>Waar staat het recessieve* allel* voor?<br/>
                              <input type="text" name="recessive_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                              <label>Is het een ziekte?<br/>
                              <select name="sickness_''' + str(count+1) + '''">
                                <option value="yes">Ja</option>
                                <option value="no">Nee</option>
                              </select></label><br/>
                          </fieldset>''')

            print('''
                          <input id="submit" type="submit" value="Submit" name="submit"/>
                        </div>
                      </form>''')
        else:
            mother = self.mother.genotype
            alleles = self.web_args
            # there are two non-trait related elements in web_args, so that's why -2.
            # and for every trait, there are four things in web_args:
            # trait name, dominant allele, recessive allele and if it's a sickness,
            # so that's why /4.
            for i in range(int((len(alleles) - 2) / 4)):
                trait = "trait_" + str(i + 1)
                dominance = "dominance_" + str(i + 1)
                recessive = "recessive_" + str(i + 1)
                print('''
                    <div class="traits">
                      <p>Eigenschap ''' + sorted(list(set(mother.upper())))[i] +
                      ''' is ''' + alleles.getvalue(trait) + ''':<br/>
                        Dominant* is ''' + alleles.getvalue(dominance) + '''.<br/>
                        Recessief* is ''' + alleles.getvalue(recessive) + '''.<br/>
                      </p>
                    </div>
                    ''')
            print('''
                    <div>
                      <p>Dit zijn de mogelijke fenotypen* en genotypen* voor uw kind.<br/>
                      Het kan zijn dat er in de fenotype* piechart staat "onbekend of het kind een
                      bepaalde ziekte heeft". Dit komt dan doordat er uit de meegegeven dominante*
                      en recessieve* allelen* niet kon worden opgemaakt of het om een ziek of gezond
                      fenotype* ging.</p>''')
            self.print_gametes()  # prints gametes in text
            self.write_piechart_scripts()  # writes scripts that convert the results to piecharts.
            print('''
                <div id="piechart_f"></div><div id="piechart_g"></div>
                <p><br/>*Deze begrippen worden uitgelegd op de pagina "mendel"</p>
                </div>
             ''')
        print('''
                </div>
              </body>
            </html>
            ''')
        sys.exit()


# MAIN
def main():
    """
    Takes the program through all the necessary processes.
    """
    if len(cgi.FieldStorage()) == 0:  # no web input, use commandline calculator.
        commandline_calculator.CommandlineCalculator()
    else:  # web input, use web calculator
        WebCalculator()
    return 0


# ENTRY POINT
if __name__ == "__main__":
    sys.exit(main())
